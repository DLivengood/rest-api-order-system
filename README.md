Start server with the command `./gradlew bootRun`

REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
POST | /shutdown | | | Shuts down the server
GET | /customers/all | | [{ "number" : *number*,"firstName" : *firstName*,"lastName" : *lastName*}, ...] | Gets all customers in database
PATCH | /customers/{number}/update | {"firstName" : *firstName*,"lastName" : *lastName*} | {"number" : *number*,"firstName" : *firstName*,"lastName" : *lastName*} | Updates a customer with new first and last name.
GET | customers/{lastName}/{firstName} | | Customer Number: *number* | Get customer's number from name.
GET | orders/customers/{number} | | [*orderNumber*, ...] | Get all of a customer's order's numbers.
DELETE | /orders/{number}/empty | | Order: *number* emptied | Empty an order of all product lines
GET | /orders/{number}/form | | Order Number: *orderNumber* <br>Customer:<br>#: *customerNumber*<br>Name: *firstName* *lastName*<br>Products:<br>SKU: *productSku*	Quantity: *quantity*	Price Total: *Product line price total*<br>...<br><br>Order total Price:	*totalOrderPrice* | Get the order form (includes customer #, name, product lines and total prices.